Compile:     gcc -g -Wall -o main main.c -lpthread

Run:         ./main <buffer_size> <number_of_consumer_thread> <number_of_producer_thread>


	      1. For one producer one consumer problem,
                 RUN this program "./main 10 1 1"

              2. For one producer multiple consumer problem,
                 RUN this program "./main 10 3 1"

              3. For multiple producer multiple consumer problem,
                 RUN this problem "./main 10 3 3"


The program runs forever but functions sleep 1 second to see the output.
If you works on Terminal please Ctrl+C for exit the program.


Thanks....





