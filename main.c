/**
 *
 * Author:      Ebubekir Yiğit
 *
 * File:        main.c
 *
 * Purpose:     Producer Consumer problem
 *
 *              In our case our producer will produce an item and write it in a buffer
 *              for the consumer. Then the consumer will read the item from the buffer.
 *
 * Compile:     gcc -g -Wall -o main main.c -lpthread
 *
 * Run:         ./main <buffer_size> <number_of_consumer_thread> <number_of_producer_thread>
 *
 *              All of them must be -- unsigned int --
 *
 *
 * Input:       none
 *
 * Output:      READ or WRITE mode
 *              Which thread reads/writes buffer
 *              Buffer index and buffer element
 *
 *
 * Notes:
 *
 *              1. For one producer one consumer problem,
 *                 RUN this program "./main 10 1 1"
 *
 *              2. For one producer multiple consumer problem,
 *                 RUN this program "./main 10 3 1"
 *
 *              3. For multiple producer multiple consumer problem,
 *                 RUN this problem "./main 10 3 3"
 *
 *
 */

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <semaphore.h>
#include <unistd.h>         // for sleep


pthread_mutex_t Lock;

sem_t full,empty;

long consumer_thread;
long producer_thread;
long consumer_thread_count;
long producer_thread_count;
pthread_t *consumer_thread_handles;
pthread_t *producer_thread_handles;


int index = 0;
int* buffer;        // buffer integer array
long buffer_size;


void *consumerThread(void* rank);
void *producerThread(void* rank);
int pickRandomInteger(int sizeN);



int main(int argc, char *argv[]){

    /* Wrong program argument size, Write program format and exit... */
    if (argc != 4){
        printf("***************\nWrong argument size !\nProgram format is: <buffer_size><consumer_thread_count><producer_thread_count>\n*****************\n");
        return 0;
    }

    buffer_size = atoi(argv[1]);
    consumer_thread_count = atoi(argv[2]);
    producer_thread_count = atoi(argv[3]);

    printf("The program works for   -->>  \nBuffer size = %li  ---  Consumer size = %li  ---  Producer size = %li\n\n\n",buffer_size,consumer_thread_count,producer_thread_count);

    producer_thread_handles = malloc(producer_thread_count * sizeof(pthread_t));
    consumer_thread_handles = malloc(consumer_thread_count * sizeof(pthread_t));

    buffer = calloc((size_t) buffer_size, sizeof(int));


    /* Initialize semaphores */
        sem_init(&empty, 0, (unsigned int) buffer_size);
        sem_init(&full, 0, 0);


    /* Initialize mutex Lock */
    pthread_mutex_init(&Lock,NULL);


    /* Create producer threads */
    for(producer_thread = 0; producer_thread < producer_thread_count; producer_thread++){
        pthread_create(&producer_thread_handles[producer_thread],NULL,producerThread,(void*) producer_thread);
    }
    /* Crate consumer threads */
    for(consumer_thread = 0; consumer_thread < consumer_thread_count; consumer_thread++){
        pthread_create(&consumer_thread_handles[consumer_thread],NULL,consumerThread,(void*) consumer_thread);
    }



    /* Join producer threads for exit */
    for(producer_thread = 0; producer_thread < producer_thread_count; producer_thread++){
        pthread_join(producer_thread_handles[producer_thread],NULL);
    }
    /* Join consumer threads for exit */
    for(consumer_thread = 0; consumer_thread < consumer_thread_count; consumer_thread++){
        pthread_join(consumer_thread_handles[consumer_thread],NULL);
    }
    

    /* Destroy semaphores */
    sem_destroy(&empty);
    sem_destroy(&full);

    return  0;

}

/**
 * ------------------------------------------------------------
 * called number of producer_thread_count times
 * locks buffer and writes an integer and unlocks buffer
 *
 * @param rank  (thread index)
 * @return
 */
void* producerThread(void* rank){
    long my_rank = (long) rank;
    while(1){
        int inner = pickRandomInteger(100) + 1;
        sem_wait(&empty);
        pthread_mutex_lock(&Lock);
        if (index < buffer_size){
            buffer[index] = inner;
            printf("WRITE : : : Buffer[ %d ] = %d  -- written by thread %li\n",index,inner,my_rank);
            index++;
        }
        pthread_mutex_unlock(&Lock);
        sem_post(&full);
        sleep(1);
    }

    printf("Producer thread  [ %li ]\n",my_rank);
    return NULL;
}

/**
 * -----------------------------------------------------------------
 * called number of consumer_thread_count times
 * locks buffer and reads it and unlocks buffer
 *
 * @param rank (thread index)
 * @return
 */
void* consumerThread(void* rank){
    long my_rank = (long) rank;
    while(1){
        sem_wait(&full);
        pthread_mutex_lock(&Lock);
        if (index > 0){
            int inner = buffer[index - 1];  // reads from buffer
            printf("READ : : : Buffer[ %d ] = %d  -- read by thread %li\n",index - 1,inner,my_rank);
            index--;
        }
        pthread_mutex_unlock(&Lock);
        sem_post(&empty);
        sleep(1);
    }

    printf("Consumer thread  [ %li ]\n",my_rank);
    return NULL;
}

/**
 * ------------------------------------------------------------
 * @param sizeN
 * @return  returns random integer between 0-sizeN
 */
int pickRandomInteger(int sizeN){
    int random = rand() % sizeN;
    return random;
}
